To Update cmake_tdaq:

1. Merge with 

https://gitlab.cern.ch/atlas-tdaq-software/cmake_tdaq

2. Clone

https://gitlab.cern.ch/atlas-tdaq-software/tdaq-common-cmake.git

and

cp tdaq-common-cmake/cmake/*.cmake cmake
cp tdaq-common-cmake/cmake/modules/*.cmake cmake/modules
