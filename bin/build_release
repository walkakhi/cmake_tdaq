#!/bin/bash
#
# Usage: build_release <project> <branch> <configuration> [ CMake options -- [ CTest Options ]]
# e.g. 
#
#    build_release tdaq-common tdaq-common-nightly x86-64-slc6-opt
#
# The script expects a git project called
# <project>-cmake at gitlab.cern.ch
#
# It will checkout the specified <branch> of that project.
#
# You can set the CHECKOUT_AREA and/or BUILD_AREA environment variables
# to determine where source is checked out and the where the build is done.
#
# Additional parameters are put into CMAKE_OPTIONS to be used during the cmake configuration
# step. Any paramaters after -- are simply forwarded to the ctest driver
#


usage()
{
    echo "usage: $0 [--rpm] [--tar] <project> <branch> <configuration> [ cmake options ... [ -- ctest options...]]"
    echo " Options:"
    echo "       -h|--help : print this help and exit"
    echo "       --rpm     : build RPMs"
    echo "       --tar     : build tar file"
    echo "       --checkout=<path> : override checkout area from environment"
    echo "       --build=<path>    : override build area from environment"
    echo "       --clean   : clean the installed area before build"
    echo
    echo " <project>       - one of tdaq-common or tdaq. Must have a corresponding <project>-cmake in gitlab."
    echo " <branch>        - the branch to checkout. E.g. tdaq-common-99-00-00."
    echo " <configuration> - the build configuration, e.g. x86_64-centos7-gcc8-opt"
    echo
    echo " Any argument after the mandatory ones is passed to either CMake or - after '--' CTest "
}

build_rpms=0
build_tar=0

export CMAKE_OPTIONS=

while [ $# -gt 0 ]
do
    case $1 in
        --rpm)
               build_rpms=1
               shift
               ;;
        --tar)
               build_tar=1
               shift
               ;;
        --here)
            CHECKOUT_AREA=`pwd`
            BUILD_AREA=`pwd`/build
            shift
            ;;
        --checkout=*)
            CHECKOUT_AREA=${1#--checkout=}
            shift
            ;;
        --build=*)
            BUILD_AREA=${1#--build=}
            shift
            ;;
        --package-file=*)
            pkg_file="${1#--package-file=}"
            pkg_file=$(readlink -f ${pkg_file})
            export CHECKOUT_OPTIONS="${CHECKOUT_OPTIONS} --package-file=${pkg_file}"
            shift
            ;;
        --packages=*)
            export CHECKOUT_OPTIONS="${CHECKOUT_OPTIONS} \"$1\""
            shift
            ;;
        --variant=*)
            export CHECKOUT_OPTIONS="${CHECKOUT_OPTIONS} $1"
            CMAKE_OPTIONS="${CMAKE_OPTIONS} -DTDAQ_VARIANT=${1#--variant=}"
            shift
            ;;
       --clean)
            CLEAN=yes
            shift
            ;;
       -h|--help)
               usage
               exit 0
               ;;
        *)
               break
               ;;
    esac
done

if [ $# -lt 3 ]; then
    usage
    exit 1
fi

export PROJECT=$1
export BRANCH=$2
export CONFIG=$3

shift 3

export CTEST_OPTIONS=
base=${CHECKOUT_AREA}/${PROJECT}/${BRANCH}
while [ $# -gt 0 ]
do
    case $1 in
        --)
            shift
            break
            ;;
        -DCTEST_*)
            CTEST_OPTIONS="${CTEST_OPTIONS} $1"
            shift
            ;;
        CTEST_*)
            CTEST_OPTIONS="${CTEST_OPTIONS} -D $1"
            shift
            ;;
        *)
            CMAKE_OPTIONS="${CMAKE_OPTIONS} $1"
            case $1 in
                CMAKE_INSTALL_PREFIX=*)
                    base=$(dirname ${1#CMAKE_INSTALL_PREFIX=})
                    ;;
                -DCMAKE_INSTALL_PREFIX=*)
                    base=$(dirname ${1#-DCMAKE_INSTALL_PREFIX=})
                    ;;
                LCG_VERSION_CONFIG=LCG_999*)
                    setup_args=--lcg=${1#LCG_VERSION_CONFIG=LCG_999}
                    ;;
                -DLCG_VERSION_CONFIG=LCG_999*)
                    setup_args=--lcg=${1#-DLCG_VERSION_CONFIG=LCG_999}
                    ;;
            esac
            shift
            ;;
    esac
done

CMAKE_TDAQ=$(dirname $(dirname $(readlink -f ${0})))

case ${CONFIG} in
   *-opt)
      BUILD_CONFIG=Release
      ;;
   *-dbg)
      BUILD_CONFIG=Debug
      ;;
   *)
      echo "Unknown type of configuration: $3"
      exit 1
esac

export CHECKOUT_AREA=${CHECKOUT_AREA:=$(pwd)}
export BUILD_AREA=${BUILD_AREA:=${CHECKOUT_AREA}/build}
export CHECKOUT_OPTIONS

source ${CMAKE_TDAQ}/bin/setup.sh ${setup_args} ${CONFIG}
export  PYTHONDONTWRITEBYTECODE=1
if [ -n "${CLEAN}" ]; then
  rm -rf ${CHECKOUT_AREA}/${PROJECT}/${BRANCH}/installed
fi
ctest -S ${CMAKE_TDAQ}/cmake/scripts/ReleaseBuild.cmake --timeout 600 ${CTEST_OPTIONS} $@ >& log.out
result=$?

#pkill -P $$
#pkill -9 -P $$
if [ $(whoami) == "atdsoft" ]; then
for p in $(ps -u atdsoft -o pid=); do $(readlink /proc/$p/exe | grep -q "tdaq-99-00-*/installed") && (kill $p; sleep 3; kill -0 $p && kill -9 $p) ; done
fi

# this can not deal with a re-defined install prefix yet
if [ ${build_tar} -eq 1 ]; then
  mkdir -p ${BUILD_AREA}/tar || exit 1
  tar -C ${base} --exclude=.git -zcf ${BUILD_AREA}/tar/$(basename ${base})-${CONFIG}.tar.gz installed
fi

# this one can, however
if [ ${build_rpms} -eq 1 ]; then
  cd ${BUILD_AREA}/${PROJECT}/${BRANCH}/${CONFIG} || exit 1
  cpack >& pack.out
  # mkdir -p RPM/${CONFIG} RPM/noarch RPM/src
  # mv *${CONFIG}*.rpm RPM/${CONFIG}
  # mv *noarch*.rpm RPM/noarch
  # mv *.rpm RPM/src
fi

exit ${result}
