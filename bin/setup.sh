# Usage: source setup.sh [config]
#
# where 'config' is using the standard syntax: ARCH-OS-COMPILER-BUILD
# e.g. x86_64-centos7-gcc11-opt, x86_64-centos7-gcc12-opt
#

############## configuration variables ################
# You can override this in your environment for testing other versions.

#
# FELIX Package versions
#
export FELIX_BOOSTRAP_VERSION=3.3.7
export FELIX_CATCH_VERSION=3.3.2
export FELIX_CONCURRENTQUEUE_VERSION=1.0.0.99
export FELIX_CZMQ_VERSION=3.0.2.4
export FELIX_DATATABLES_VERSION=1.10.13
export FELIX_DOCOPT_VERSION=0.6.3
export FELIX_DRIVERS_VERSION=4.14.0
export FELIX_FONTAWESOME_VERSION=4.7.0
export FELIX_HIGHCHARTS_VERSION=6.1.1
export FELIX_JQUERY_VERSION=1.12.4
export FELIX_JSON_VERSION=3.9.1
export FELIX_JWRITE_VERSION=1.2.3
export FELIX_LIBCURL_VERSION=7.77.0
export FELIX_LIBFABRIC_VERSION=1.17.1
export FELIX_LIBNUMA_VERSION=2.0.12
export FELIX_LOGRXI_VERSION=0.1.1
export FELIX_MATHJS_VERSION=5.1.1
export FELIX_MOMENT_VERSION=2.24.0
export FELIX_PATCHELF_VERSION=0.9
export FELIX_PYBIND11_VERSION=2.10.4
export FELIX_READERWRITERQUEUE_VERSION=1.0.0.99
export FELIX_SIMDJSON_VERSION=3.1.6
export FELIX_SIMPLEWEBSERVER_VERSION=3.1.1
export FELIX_SPDLOG_VERSION=0.17.0
export FELIX_YAMPCPP_VERSION=0.6.3
export FELIX_ZYRE_VERSION=1.1.0.2

#
# Specific versions for LCG libraries, need to be the same as distributed in the cvmfs-felix standalone distribution
#
export FELIX_SQLITE_VERSION=3320300
export FELIX_TBB_VERSION=2020.2
export FELIX_BOOST_VERSION=1.78.0
export FELIX_PYTHONLIBS_VERSION=3.7.6
export FELIX_ZEROMQ_VERSION=4.3.4p1
export FELIX_LIBSODIUM_VERSION=1.0.18

#
#
#
export REGMAP_VERSION=${REGMAP_VERSION:=0x0500}
case "${REGMAP_VERSION}" in
    0x0400)
        export REGMAP_FW=rm4
        ;;
    *)
        export REGMAP_FW=rm5
        ;;
esac

# Latest default if release not defined otherwise.
#
# If we have sourced the run-time setup via cm_setup.sh, the
# TDAQ_LCG_RELEASE defines what we use.
lcg=${TDAQ_LCG_RELEASE:-LCG_102b}
CMAKE_VERSION=${CMAKE_VERSION:=3.23.1}
export LCG_VERSION=102
export LCG_VERSION_POSTFIX=b
# NOTE also change postfix in FELIX.cmake around line 70
export LCG_FULL_VERSION=${LCG_VERSION}${LCG_VERSION_POSTFIX}
export LCG_QT_VERSION=${LCG_FULL_VERSION}
export QT5_VERSION=5.15.2
export GDB_VERSION=11.2

export TDAQ_BASE=${TDAQ_BASE:=/cvmfs/atlas.cern.ch/repo/sw/tdaq}

# This can still be overriden on the command line...
case "$1" in
    --lcg=*)
        lcg=${1#--lcg=}
        shift
        ;;
    *)
        ;;
esac

# Determine <arch>-<os>, used to find various paths for basic binutils and compilers
# without the need for the <compiler>-<opt|dbg> part of the tag.
export TDAQ_HOST_ARCH=${TDAQ_HOST_ARCH:=$(uname -i)-$(/bin/sh -c '. /etc/os-release; echo ${ID}${VERSION_ID/\.*/}' | tr -d \. | sed -e 's;rocky;centos;' -e 's;rhel;centos;' -e 's;almalinux;centos;' -e 's;ol;centos;' )}


############## configuration variables ################
# You can override this in your environment for testing other versions.

# legacy standard AFS location of LCG software
DEFAULT_LCG_BASE="/afs/cern.ch/sw/lcg/releases"
# favour new new standard location if CVMF available
[ -d /cvmfs/sft.cern.ch/lcg/releases ] && DEFAULT_LCG_BASE="/cvmfs/sft.cern.ch/lcg/releases"

case "${lcg}" in
    dev*)
       DEFAULT_LCG_BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"
       ;;
esac

# you can still override this defining LCG_RELEASE_BASE beforehand
export LCG_RELEASE_BASE=${LCG_RELEASE_BASE:=${DEFAULT_LCG_BASE}}
export LCG_BASE=$(dirname ${LCG_RELEASE_BASE})
# export LCG_BASE=${LCG_BASE:=/cvmfs/sft.cern.ch/lcg}

# The location of LCG contrib and external
EXTERNAL_BASE=${EXTERNAL_BASE:=${LCG_BASE}/external}
CONTRIB_BASE=${CONTRIB_BASE:=${LCG_BASE}/contrib}
TOOL_BASE=${TOOL_BASE:=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/${TDAQ_HOST_ARCH}}

# The location and version of CMake to use
CMAKE_BASE=${CMAKE_BASE:=${TOOL_BASE}/CMake}
CMAKE_PATH=${CMAKE_PATH:=${CMAKE_BASE}/${CMAKE_VERSION}/bin}

# For TDAQ projects
#CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH:=/cvmfs/atlas.cern.ch/repo/sw/tdaq}

# export JAVA_HOME=${JAVA_HOME:=${EXTERNAL_BASE}/Java/JDK/1.8.0/amd64}

############## end of configuration variables ################
if [ -z "${1}" -o  "${1}" == "--" ]
then
    # no binary tag
    export BINARY_TAG=${BINARY_TAG:=${TDAQ_HOST_ARCH}-gcc11-opt}
else
    export BINARY_TAG=${1}
    shift
fi

echo "Setting up FELIX (developer)"
echo "- REGMAP: ${REGMAP_VERSION}"
echo "- TDAQ_HOST_ARCH: ${TDAQ_HOST_ARCH}"
echo "- BINARY_TAG: ${BINARY_TAG}"
echo "- LCG_FULL_VERSION: ${LCG_FULL_VERSION}"

if [ ! -d "${LCG_BASE}" ]; then
    echo "LCG_BASE Directory Not Found: ${LCG_BASE}"
    return 1
fi


# Setup path for CMAKE
export PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})):${CMAKE_PATH}:${PATH}

# Choose compiler, this has to reflect what you choose in the tag to
# cmake_config later
_conf=${BINARY_TAG}
case "${_conf}" in
    *-gcc*)
        if [ -n "${lcg}" ] && [ -f "${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt" ]; then
            gcc_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
        else
            gcc_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'gcc')
        fi
        if [ -f ${CONTRIB_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${CONTRIB_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh
        elif [ -f ${CONTRIB_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${CONTRIB_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh
        else
            gcc_version=$(g++ --version | head -1 | awk '{ print $NF; }' | cut -d. -f1)
            if [ "${gcc_version}" -ne $(echo "${gcc_requested}" | cut -d. -f1) ]; then
               echo "warning: gcc version ${gcc_requested} needed, but version ${gcc_version} found" 1>&2
            fi
        fi
        unset gcc_requested gcc_version
        source ${LCG_BASE}/releases/LCG_${LCG_FULL_VERSION}/gdb/${GDB_VERSION}/${BINARY_TAG}/gdb-env.sh 
        ;;
    *-clang*)
        if [ -n "${lcg}" ] && [ -f ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt ]; then
            clang_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
        else
            clang_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'clang')
        fi
        source ${CONTRIB_BASE}/clang/${clang_requested}/${TDAQ_HOST_ARCH}/setup.sh
        unset clang_requested
        ;;
    *)
        #default
        source ${CONTRIB_BASE}/gcc/8binutils/${TDAQ_HOST_ARCH}/setup.sh


        ;;
esac

export gcc_config_version

# For FELIX
export PATH=${LCG_BASE}/releases/LCG_${LCG_FULL_VERSION}/Python/${PYTHON_VERSION}/${BINARY_TAG}/bin:${PATH}

export SOURCE_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))/../..
export BINARY_TAG_PATH=${SOURCE_PATH}/${BINARY_TAG}
export EXTERNAL_PATH=${SOURCE_PATH}/external

# Setup of PATH to find most binaries
export PATH=${BINARY_TAG_PATH}/felix-starter:${PATH}
export PATH=${BINARY_TAG_PATH}/felix-star:${PATH}
export PATH=${BINARY_TAG_PATH}/ftools:${PATH}
export PATH=${BINARY_TAG_PATH}/elinkconfig:${PATH}
export PATH=${BINARY_TAG_PATH}/flxcard:${PATH}
export PATH=${BINARY_TAG_PATH}/felixbus-client:${PATH}
export PATH=${BINARY_TAG_PATH}/fatcat:${PATH}

# Setup LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-bus-fs:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-client:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-client-thread:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-mapper:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-star:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixbase:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixbus:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixcore:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixpy:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/flxcard:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/ftools:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/hdlc_coder:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/netio:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/netio-next:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/packetformat:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/regmap:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/tdaq_tools:${LD_LIBRARY_PATH}

export LD_LIBRARY_PATH=${EXTERNAL_PATH}/felix-drivers/${FELIX_DRIVERS_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/czmq/${FELIX_CZMQ_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/docopt/${FELIX_DOCOPT_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/jwrite/${FELIX_JWRITE_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/libfabric/${FELIX_LIBFABRIC_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/libnuma/${FELIX_LIBNUMA_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/logrxi/${FELIX_LOGRXI_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/simdjson/${FELIX_SIMDJSON_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/yaml-cpp/${FELIX_YAMPCPP_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${EXTERNAL_PATH}/zyre/${FELIX_ZYRE_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}

# setup for felix python tools
export PYTHONPATH=${BINARY_TAG_PATH}:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-def:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-unit-test:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/bus:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/config:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/elink:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/fid:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/ip:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/mode:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-client:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-config:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felixbus:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-bus-fs:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/flxcard_py:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/hdlc_coder:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/netio-next:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/data_transfer_tools:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-tag:${PYTHONPATH}

# fix from FLX-272, QT setup, no keyboard activity
export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb
export QT5_DIR=${LCG_BASE}/releases/LCG_${LCG_QT_VERSION}/qt5/${QT5_VERSION}/${BINARY_TAG}
export QT_QPA_PLATFORM_PLUGIN_PATH=${QT5_DIR}/plugins/platforms
export LD_LIBRARY_PATH=${QT5_DIR}/lib:${LD_LIBRARY_PATH}


# Setup CMAKE_PREFIX_PATH to find LCG
export CMAKE_PREFIX_PATH="${CMAKE_PROJECT_PATH} $(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})))/cmake"

unset CMAKE_BASE CMAKE_VERSION CMAKE_PATH GCC_BASE EXTERNAL_BASE CONTRIB_BASE TOOL_BASE _conf lcg
unset SOURCE_PATH BINARY_TAG_PATH EXTERNAL_PATH

echo "- PYTHON: `which python`"

# Run-time variable for RDMA
export FI_VERBS_TX_IOV_LIMIT=30
export FI_VERBS_RX_IOV_LIMIT=30
export FI_VERBS_TX_SIZE=1024
export FI_VERBS_RX_SIZE=1024
export RDMAV_FORK_SAFE=1
