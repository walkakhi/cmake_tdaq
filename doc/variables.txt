
ENABLE_PACKAGES   default: *
DISABLE_PACKAGES  default: ""
   These two variables can be set in the top-level CMakeLists.txt of the work area *before*
   calling tdaq_work_area() or tdaq_project(). They will explicitly enable or disable locally
   checked out packages. By default all top level directories that have a CMakeLists.txt file
   are considered enabled packages.

TDAQ_CMAKE_DIR 
   The location of the cmake_tdaq version that is used. It can be used to find
   needed scripts or other files from that package.

TDAQ_DB_PROJECT - default: 'daq'
   The subdirectory where OKS files are installed, i.e. <install_area>/share/data/${TDAQ_DB_PROJECT}/schema etc.
   Can be overriden in a dependent project.

TDAQ_DATA_URL - default: "http://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/data"
   The location from where tdaq_get_data() tries to download data files at build time
   if no explicit full URL is given.

TDAQ_PACKAGE_NAME
TDAQ_PACKAGE_VERSION
   Name and version of the 'current' package. Injected into the scope by tdaq_package(). The name
   is simply the directory name of the package, the version is determined via git or snv or is 'UNKNONW'
   otherwise.

   In addition -DTDAQ_PACKAGE_NAME=${TDAQ_PACKAGE_NAME} is added to the C++ preprocessor flags for
   the package, so it can be used as a definition in the source code.

   The following derived variables are also available but should usually be avoided:
   <package>_SOURCE_DIR, <package>_BINARY_DIR

TDAQ_HAVE_<package>
   This variable is defined for each <package> that is checked out and enabled in the current work
   area. This should be used 

TDAQ_PACKAGE_LABELS
   Set by default to the package name. More labels can be added with 
   tdaq_add_package_labels(label1 label2...). These are propagated to the tests defined
   in the package. 

TDAQ_ALL_TARGETS
   After all packages have been configured this variable will contain a list of all targets.
   This excludes targets that have not been defined with one of the tdaq_xxxx() wrapper commands.
   Only used internally for post-processing the generated <project>Targets.* files.

TDAQ_FORTRAN_LANGUAGE
   Set this before calling tdaq_project() or tdaq_work_area() if you use Fortran. It will
   enable Fortran for the project.

   set(TDAQ_FORTRAN_LANGUAGE Fortran)

   Note: nobody seems to use this, for potential backward compatibility.

USE_CCACHE
   When defined at configuration time as a parameter to cmake, will enable the use of ccache.
   Check $HOME/.ccache for configuration files first...

   cmake -DUSE_CCACHE=ON ...


PROJECT_NAME
PROJECT_VERSION*
   Contain the current project name and version in CMake format. Note that in a work
   area the name is 'work-${project}' where ${project} is the installed release name while
   the version is the same as the release.

TDAQ_PROJECT_NAME
   contains the current project name and version in 'TDAQ-format', e.g. tdaq-08-01-02

