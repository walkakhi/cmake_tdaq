#
# This is a version of the ctest build script that can be used in a
# container and a gitlab CI job.
#
# It assumes that the source code has been checked out in the current
# directory and that the CMTCONFIG environment variable contains
# the desired build environment, as well as that the proper
# environment is setup.
#
# CMAKE_OPTIONS: from environment
# CMTCONFIG: from environment
#
# other ctest options (variables) and their defaults
#
# CTEST_BUILD_NAME: $ENV{CI_COMMIT_REF_NAME}-${CMTCONFIG}
# CTEST_MODE : Experimental
# CTEST_TRACK: Experimental
# CTEST_NOCLEAN: FALSE
# CTEST_NOTESTS: FALSE
#
#
set(CTEST_CONFIG $ENV{CMTCONFIG})
set(CTEST_CMAKE_GENERATOR  "Unix Makefiles")

get_filename_component(CTEST_SOURCE_DIRECTORY "." ABSOLUTE)
get_filename_component(CTEST_BINARY_DIRECTORY "./${CTEST_CONFIG}" ABSOLUTE)

if(NOT DEFINED CTEST_BUILD_NAME)
set(CTEST_BUILD_NAME "$ENV{CI_COMMIT_REF_NAME}-${CTEST_CONFIG}")
endif()

set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")

if(NOT DEFINED CTEST_MODE)
set(CTEST_MODE Experimental)
endif()

if(NOT DEFINED CTEST_NOCLEAN AND EXISTS ${CTEST_BINARY_DIRECTORY})
ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
endif()

if(DEFINED CTEST_TRACK)
set(CTEST_TRACK TRACK ${CTEST_TRACK})
endif()

find_program(hostname NAMES hostname)
execute_process(COMMAND ${hostname} OUTPUT_VARIABLE CTEST_SITE OUTPUT_STRIP_TRAILING_WHITESPACE)

include(ProcessorCount)
ProcessorCount(NumCores)

if(${NumCores} GREATER 0)
set(CTEST_BUILD_FLAGS "-j ${NumCores}")
endif()

find_program(make NAMES make)
set(CTEST_BUILD_COMMAND "${make} ${CTEST_BUILD_FLAGS} -k all install/fast")
set(CONFIG_OPTIONS "-DBINARY_TAG=${CTEST_CONFIG}" $ENV{CMAKE_OPTIONS})

ctest_start(${CTEST_MODE} ${CTEST_TRACK})
ctest_read_custom_files(${CTEST_SOURCE_DIRECTORY}/cmake)
ctest_configure(OPTIONS "${CONFIG_OPTIONS}" QUIET)
ctest_read_custom_files(${CTEST_BINARY_DIRECTORY}/cmake)
ctest_build(QUIET)
if(NOT DEFINED CTEST_NOTESTS)
ctest_test(QUIET)
endif()

if(EXISTS ${CTEST_BINARY_DIRECTORY}/packages.txt)
  set(CMAKE_EXTRA_SUBMIT_FILES ${CMAKE_EXTRA_SUBMIT_FILES} ${CTEST_BINARY_DIRECTORY}/packages.txt)
  file(STRINGS ${CTEST_BINARY_DIRECTORY}/packages.txt packages)
endif()

# get_filename_component(bindir ${cmake_config} DIRECTORY)
# get_filename_component(cmake_tdaq_dir ${bindir}/.. ABSOLUTE)

# find_program(head NAMES head)
# execute_process(COMMAND ${head} -1  ${CTEST_BINARY_DIRECTORY}/Testing/TAG OUTPUT_VARIABLE tagdir OUTPUT_STRIP_TRAILING_WHITESPACE)
# find_program(post_build NAMES post_build.py HINTS ${cmake_tdaq_dir}/cmake/scripts NO_SYSTEM_DEFAULT)
# execute_process(COMMAND ${post_build} ${CTEST_BINARY_DIRECTORY}/Testing/${tagdir}/Build.xml ${CTEST_SOURCE_DIRECTORY} ${CTEST_BINARY_DIRECTORY} OUTPUT_VARIABLE perPackage)
# file(WRITE ${CTEST_BINARY_DIRECTORY}/PerPackage.html ${perPackage})
# set(CMAKE_EXTRA_SUBMIT_FILES ${CMAKE_EXTRA_SUBMIT_FILES} ${CTEST_BINARY_DIRECTORY}/PerPackage.html)

ctest_upload(FILES ${CMAKE_EXTRA_SUBMIT_FILES} QUIET)

ctest_submit(QUIET RETRY_COUNT 3)
