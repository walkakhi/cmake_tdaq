#!/usr/bin/env python
# usage:
#   Note: run from lcg/release area
#
#   update_links LCG_97rc4 x86_64-centos7-gcc8-opt
#
from __future__ import print_function

import sys
import os

try:
    import urllib
    from urllib.request import urlopen
except:
    import urllib2
    from urllib2 import urlopen

if len(sys.argv) < 3:
    print("usage: ",sys.argv[0],"<LCG_RELEASE> <BUILD_CONFIGURATION> [BUILD_INFO_URL]")
    sys.exit(1)

release  = sys.argv[1]
platform = sys.argv[2]

url = len(sys.argv) > 3 and sys.argv[3] or 'http://lcgpackages.web.cern.ch/lcgpackages/lcg/meta/'

packages = {}

for line in urlopen(url + release +'_' + platform + '.txt'):
    if len(line) == 0 or line[0] == '#':  
        continue
    args = line.split(',',10)
    d = {}
    for arg in args:
        try:
            key,value = arg.split(':',1)
        except:
            print(arg,file=sys.stderr)
            break
        d[key.strip()] = value.strip()
    packages[d['NAME']] = d

if not os.path.isdir(release):
    os.mkdir(release)

externals = file(release + '/LCG_externals_' + platform + '.txt','w')
generators = file(release + '/LCG_generators_' + platform + '.txt', 'w')

cxx_flavor, cxx_version = packages.values()[0]['COMPILER'].split(' ')
if cxx_flavor == 'GNU':
   cxx = 'gcc;' + cxx_version
else:
   cxx = 'clang;' + cxx_version

for f in [ externals, generators ]:
    print('PLATFORM:', platform, file=f)
    print('VERSION:', release, file=f)
    print('COMPILER:', cxx, file=f)

candidates = os.listdir('.')
if os.path.isdir('MCGenerators'):
    candidates.extend(os.listdir('MCGenerators'))

for dir in candidates:
    if dir in packages:
        # check if ./<package>/<hash>-<version>/$CMTCONFIG exists
        check = packages[dir]['DIRECTORY'] + '/' + packages[dir]['VERSION']+'-'+packages[dir]['HASH']+'/' + platform
        if os.path.isdir(check):

            # yes, check if the target directory exists
            target = release + '/' + packages[dir]['DIRECTORY'] + '/' + packages[dir]['VERSION'] + '/' + platform
            if not os.path.isdir(target):

                parent = release + '/' + packages[dir]['DIRECTORY'] + '/' + packages[dir]['VERSION']

                # Check if intermediate directories exists, create if not
                if not os.path.isdir(parent):
                    os.makedirs(parent)

                # calculate relative path
                rel = os.path.relpath(check, parent)
                os.symlink(rel, target)

            if packages[dir]['DIRECTORY'].startswith('MCGenerators'):
                f = generators
            else:
                f = externals

            # Write to LCG_[externals|generators]_<platform>.txt
            print(dir,
                packages[dir]['HASH'],
                packages[dir]['VERSION'],
                './' + packages[dir]['DIRECTORY'] + '/' + packages[dir]['VERSION'] + '/' + platform,
                packages[dir]['DEPENDS'],
                sep='; ', file=f)
